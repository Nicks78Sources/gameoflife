program GameOfLife;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, mian, optionsunit, game, stepsDialog, fillDialog, brushes, brushEditor,
  optionsFormUnit, aboutFormUnit;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TstepsDialogForm, stepsDialogForm);
  Application.CreateForm(TfillDialogForm, fillDialogForm);
  Application.CreateForm(TBrushEditorForm, BrushEditorForm);
  Application.CreateForm(ToptionsForm, optionsForm);
  Application.CreateForm(TaboutForm, aboutForm);
  Application.Run;
end.

