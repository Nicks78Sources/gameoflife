unit mian;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids, Menus,
  ExtCtrls, ComCtrls, Spin, StdCtrls,
  optionsUnit, optionsFormUnit,
  stepsDialog, fillDialog,
  brushes,brushEditor,
  aboutFormUnit,
  game;

type

  { TMainForm }

  TMainForm = class(TForm)
    FieldsDrawGrid: TDrawGrid;
    ImageList1: TImageList;
    BrushRandomAngleMenuItem: TMenuItem;
    randomSymmetryBrushMenuItem: TMenuItem;
    ySymmetryBrushMenuItem: TMenuItem;
    xSymmetryBrushMenuItem: TMenuItem;
    symmetryBrushMenuItem: TMenuItem;
    StepsLabel: TLabel;
    BrushMenuItem: TMenuItem;
    xCoordLabel: TLabel;
    doublepointLabel: TLabel;
    yCoordLabel: TLabel;
    resetBrushMenuItem: TMenuItem;
    AngleBrushMenuItem: TMenuItem;
    BrushesBrushMenuItem: TMenuItem;
    Brush0AngleMenuItem: TMenuItem;
    Brush90AngleMenuItem: TMenuItem;
    Brush180AngleMenuItem: TMenuItem;
    Brush270AngleMenuItem: TMenuItem;
    noneBrushSetBrushBrushMenuItem: TMenuItem;
    BrushSeparator: TMenuItem;
    BrushEditorBrushMenuItem: TMenuItem;
    resetCounterPopupMenuItem: TMenuItem;
    counterPopupMenu: TPopupMenu;
    stepsCounterLabel: TLabel;
    MainMenu1: TMainMenu;
    FileMenuItem: TMenuItem;
    aboutFileMenuItems: TMenuItem;
    exitFileMenuItems: TMenuItem;
    AreaMenuItem: TMenuItem;
    clearAreaMenuItems: TMenuItem;
    stepsSpinEdit: TSpinEdit;
    stopRunMenuItems: TMenuItem;
    runRunMenuItems: TMenuItem;
    RunMenuItemsSeparator1: TMenuItem;
    stepsRunMenuItems: TMenuItem;
    stepRunMenuItems: TMenuItem;
    randomfillAreaMenuItems: TMenuItem;
    RunMenuItem: TMenuItem;
    optionsFileMenuItems: TMenuItem;
    FileMenuItemsSeparator1: TMenuItem;
    Timer1: TTimer;
    ToolBar1: TToolBar;
    runToolButton: TToolButton;
    stopToolButton: TToolButton;
    ToolButton1: TToolButton;
    stepsToolButton: TToolButton;
    ToolButton3: TToolButton;
    clearToolButton: TToolButton;
    procedure aboutFileMenuItemsClick(Sender: TObject);
    procedure BrushEditorBrushMenuItemClick(Sender: TObject);
    procedure BrushRandomAngleMenuItemClick(Sender: TObject);
    procedure clearAreaMenuItemsClick(Sender: TObject);
    procedure exitFileMenuItemsClick(Sender: TObject);
    procedure FieldsDrawGridClick(Sender: TObject);
    procedure FieldsDrawGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure FieldsDrawGridMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Brush0AngleMenuItemClick(Sender: TObject);
    procedure Brush90AngleMenuItemClick(Sender: TObject);
    procedure Brush180AngleMenuItemClick(Sender: TObject);
    procedure Brush270AngleMenuItemClick(Sender: TObject);
    procedure noneBrushSetBrushBrushMenuItemClick(Sender: TObject);
    procedure randomSymmetryBrushMenuItemClick(Sender: TObject);
    procedure someBrushMenuItemClick(Sender: TObject);
    procedure optionsFileMenuItemsClick(Sender: TObject);
    procedure randomfillAreaMenuItemsClick(Sender: TObject);
    procedure resetBrushMenuItemClick(Sender: TObject);
    procedure resetCounterPopupMenuItemClick(Sender: TObject);
    procedure runRunMenuItemsClick(Sender: TObject);
    procedure stepRunMenuItemsClick(Sender: TObject);
    procedure stepsRunMenuItemsClick(Sender: TObject);
    procedure stepsToolButtonClick(Sender: TObject);
    procedure stopRunMenuItemsClick(Sender: TObject);

    procedure applyOptions();

    procedure Timer1Timer(Sender: TObject);
    procedure gameStep();
    procedure updatePicture();

    procedure runsEnables(e : boolean);

    function addBrushIntoBrushesBrushMenuItem(brushName : String) : boolean;
    function deleteBrushFromBrushesBrushMenuItem(brushName : String) : boolean;
    procedure xSymmetryBrushMenuItemClick(Sender: TObject);
    procedure ySymmetryBrushMenuItemClick(Sender: TObject);
  private
    isSteps : boolean;
    stepsCounter : integer;

    brushNumber : integer; // -1 : no brush(default)
    brushAngle  : integer;

    procedure afterInit(Sender: TObject; var Done: Boolean);
  public

  end;

var
  MainForm: TMainForm;
  gof : GameOfLife;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.FormCreate(Sender: TObject);
var
  i : integer;
begin
    programOptions := Options.Create();
    programOptions.readOptions();

    MainForm.Height := programOptions.formHeight;
    MainForm.Width  := programOptions.formWidth;

    gof := GameOfLife.Create(programOptions.ySize,programOptions.xSize);

    applyOptions();

    GameBrushes := CellBrushes.Create();
    GameBrushes.loadFromFile();
    for i:=0 to GameBrushes.getBrushQuantity()-1 do
        addBrushIntoBrushesBrushMenuItem(GameBrushes.getBrush(i).name);
    Application.OnIdle := @self.afterInit;

    Timer1.Enabled := False;

    isSteps := False;
    stepsCounter := 0;
    brushNumber := -1;
    brushAngle := 0;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
    programOptions.saveWindowSize(MainForm.Width, MainForm.Height);

    GameBrushes.Free;
    FreeAndNil(gof);
end;

(*-----------------------------------------------------*)
// menus
(*-----------------------------------------------------*)

// file submenu
procedure TMainForm.optionsFileMenuItemsClick(Sender: TObject);
begin
    optionsForm.Show();
end;

procedure TMainForm.aboutFileMenuItemsClick(Sender: TObject);
begin
    aboutForm.Show();
end;

procedure TMainForm.exitFileMenuItemsClick(Sender: TObject);
begin
    Application.Terminate();
end;

// area submenu

procedure TMainForm.clearAreaMenuItemsClick(Sender: TObject);
begin
     gof.clearField();
     gof.resetStepsCounter();
     updatePicture();
end;

procedure TMainForm.randomfillAreaMenuItemsClick(Sender: TObject);
begin
    fillDialogForm.ShowModal();
    if (fillDialogForm.confirmation) then begin
        if (fillDialogForm.isClearFieldBeforeFill) then
            gof.clearField();
        gof.randomFill(fillDialogForm.probabilityPercents);
        updatePicture();
    end;
end;

// brush submenu

procedure TMainForm.resetBrushMenuItemClick(Sender: TObject);
begin
    // noneBrushSetBrushBrushMenuItem.Checked := True;
    noneBrushSetBrushBrushMenuItemClick(nil);
end;

procedure TMainForm.Brush0AngleMenuItemClick(Sender: TObject);
begin
    Brush0AngleMenuItem.Checked := True;
    self.brushAngle := 0;
end;

procedure TMainForm.Brush90AngleMenuItemClick(Sender: TObject);
begin
    Brush90AngleMenuItem.Checked := True;
    self.brushAngle := 90;
end;

procedure TMainForm.Brush180AngleMenuItemClick(Sender: TObject);
begin
   Brush180AngleMenuItem.Checked := True;
   self.brushAngle := 180;
end;

procedure TMainForm.Brush270AngleMenuItemClick(Sender: TObject);
begin
    Brush270AngleMenuItem.Checked := True;
    self.brushAngle := 270;
end;

procedure TMainForm.BrushRandomAngleMenuItemClick(Sender: TObject);
begin
    BrushRandomAngleMenuItem.Checked := True;
    self.brushAngle := -1;
end;

procedure TMainForm.xSymmetryBrushMenuItemClick(Sender: TObject);
begin
    xSymmetryBrushMenuItem.Checked := not xSymmetryBrushMenuItem.Checked;
    randomSymmetryBrushMenuItem.Checked := False;
end;

procedure TMainForm.ySymmetryBrushMenuItemClick(Sender: TObject);
begin
    ySymmetryBrushMenuItem.Checked := not ySymmetryBrushMenuItem.Checked;
    randomSymmetryBrushMenuItem.Checked := False;
end;

procedure TMainForm.randomSymmetryBrushMenuItemClick(Sender: TObject);
begin
    xSymmetryBrushMenuItem.Checked := False;
    ySymmetryBrushMenuItem.Checked := False;
    randomSymmetryBrushMenuItem.Checked := True;
end;

procedure TMainForm.noneBrushSetBrushBrushMenuItemClick(Sender: TObject);
begin
    noneBrushSetBrushBrushMenuItem.Checked := True;
    Brush0AngleMenuItem.Checked := True;
    xSymmetryBrushMenuItem.Checked := False;
    ySymmetryBrushMenuItem.Checked := False;
    randomSymmetryBrushMenuItem.Checked := False;
    brushNumber := -1;
    brushAngle := 0;
end;

procedure TMainForm.someBrushMenuItemClick(Sender: TObject);
var
  someMenuItem : TMenuItem;
begin
    if (Sender is TMenuItem) then begin
        someMenuItem := TMenuItem(Sender);
        someMenuItem.Checked := True;

        brushNumber := GameBrushes.getBrushNumber(someMenuItem.Caption);
    end;
end;

procedure TMainForm.BrushEditorBrushMenuItemClick(Sender: TObject);
begin
    BrushEditorForm.Show();
end;

// popup menu on steps counter label

procedure TMainForm.resetCounterPopupMenuItemClick(Sender: TObject);
begin
     gof.resetStepsCounter();
     updatePicture();
end;

// run submenu
procedure TMainForm.stepRunMenuItemsClick(Sender: TObject);
begin
    gameStep();
    MainForm.stepsCounterLabel.Repaint;
end;

procedure TMainForm.stepsToolButtonClick(Sender: TObject);
begin
    runsEnables(False);
    stepsCounter := stepsSpinEdit.Value - 1;
    gameStep();
    if (stepsCounter > 0) then begin
        isSteps := True;
        Timer1.Enabled := True;
    end
    else
        runsEnables(True);
end;

procedure TMainForm.stepsRunMenuItemsClick(Sender: TObject);
begin
    stepsDialogForm.ShowModal();
    if (stepsDialogForm.confirmation) then begin
        runsEnables(False);
        stepsCounter := stepsDialogForm.steps;
        isSteps := True;
        if (stepsDialogForm.isViewing) then begin
           Timer1.Enabled := True;
        end
        else begin
             MainForm.FieldsDrawGrid.Cursor := crHourGlass;
             while ((stepsCounter > 0) and isSteps) do begin
                 gof.step();
                 Dec(stepsCounter);
                 Application.ProcessMessages();
             end;
             MainForm.FieldsDrawGrid.Cursor := crDefault;
             updatePicture();
             runsEnables(True);
        end;
    end;
end;

procedure TMainForm.runRunMenuItemsClick(Sender: TObject);
begin
    isSteps := False;
    runsEnables(False);

    Timer1.Enabled := True;
end;

procedure TMainForm.stopRunMenuItemsClick(Sender: TObject);
begin
    Timer1.Enabled := False;
    isSteps := False;
    stepsCounter := 0;

    runsEnables(True);
end;

(*--------------------------------------------------------*)

procedure TMainForm.Timer1Timer(Sender: TObject);
begin
    gameStep();
    if (isSteps) then begin
        Dec(stepsCounter);
        if (stepsCounter = 0) then begin
            Timer1.Enabled := False;
            isSteps := False;
            runsEnables(True);
        end;
    end;
end;

procedure TMainForm.gameStep();
begin
    gof.step();
    updatePicture();
end;

(*--------------------------------------------------------*)
// draw
(*--------------------------------------------------------*)

procedure TMainForm.FieldsDrawGridClick(Sender: TObject);
var
  cur : TGridRect;
  Row,Col : integer;

  a : integer;
  xSym,ySym : boolean;
begin
    if (not runToolButton.Enabled) then exit;

    cur := FieldsDrawGrid.Selection;
    if (self.brushNumber = -1) then begin
        for Row := cur.Top to cur.Bottom do
            for Col := cur.Left to cur.Right do begin
                gof.invertFieldAt(Row,Col);
            end;
    end
    else begin
        if ((self.brushAngle = -1) and randomSymmetryBrushMenuItem.Checked) then begin
            GameBrushes.printExByCode(gof,self.brushNumber,cur.Bottom,cur.Right,Random(8));
        end
        else begin
            if (self.brushAngle = -1) then
                a := Random(4)*90
            else
                a := self.brushAngle;
            if (randomSymmetryBrushMenuItem.Checked) then begin
                if (Random(2) = 1) then xSym := True else xSym := False;
                if (Random(2) = 1) then ySym := True else ySym := False;
            end
            else begin
                xSym := xSymmetryBrushMenuItem.Checked;
                ySym := ySymmetryBrushMenuItem.Checked;
            end;
            GameBrushes.paintEx(gof,self.brushNumber,cur.Bottom,cur.Right,a,xSym,ySym);
        end;
    end;
end;

procedure TMainForm.FieldsDrawGridDrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
begin
  with Sender as TDrawGrid, Canvas do begin
    if (gof.getUnitAt(aRow,aCol) = 1) then
        Canvas.Brush.Color := programOptions.cellColor
    else
        Canvas.Brush.Color := programOptions.emptyColor;

    Canvas.FillRect(aRect);
  end;
end;

procedure TMainForm.FieldsDrawGridMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  ACol, ARow : integer;
begin
    MainForm.FieldsDrawGrid.MouseToCell(X, Y, ACol, ARow);
    MainForm.xCoordLabel.Caption := IntToStr(ACol+1);
    MainForm.yCoordLabel.Caption := IntToStr(ARow+1);
end;

procedure TMainForm.updatePicture();
begin
    MainForm.FieldsDrawGrid.Repaint();
    MainForm.stepsCounterLabel.Caption := IntToStr(gof.getNumberOfSteps());
end;

(*--------------------------------------------------------*)
// some controls manipulation

procedure TMainForm.runsEnables(e: boolean);
begin
    stepRunMenuItems.Enabled := e;
    stepsRunMenuItems.Enabled := e;
    runRunMenuItems.Enabled := e;
    clearAreaMenuItems.Enabled := e;
    randomfillAreaMenuItems.Enabled := e;

    stepsSpinEdit.Enabled := e;
    stepsToolButton.Enabled := e;
    runToolButton.Enabled := e;
    clearToolButton.Enabled := e;

    resetCounterPopupMenuItem.Enabled := e;

    stopRunMenuItems.Enabled := not e;
    stopToolButton.Enabled := not e;
end;

function TMainForm.addBrushIntoBrushesBrushMenuItem(brushName: String): boolean;
var
  subItem: TMenuItem;
begin
    subItem := TMenuItem.Create(BrushesBrushMenuItem);
    subItem.Caption := brushName;
    subItem.RadioItem := True;
    subItem.OnClick := @someBrushMenuItemClick;
    BrushesBrushMenuItem.Add(subItem);

    addBrushIntoBrushesBrushMenuItem := True;
end;

function TMainForm.deleteBrushFromBrushesBrushMenuItem(brushName: String): boolean;
var
  subItem: TMenuItem;
begin
    subItem := BrushesBrushMenuItem.Find(brushName);
    if (subItem <> nil) then begin
        if (subItem.Checked = True) then
            noneBrushSetBrushBrushMenuItem.Checked := True;
        BrushesBrushMenuItem.Delete(BrushesBrushMenuItem.IndexOf(subItem));
        self.brushNumber := -1;
        deleteBrushFromBrushesBrushMenuItem := True;
        exit;
    end;
    deleteBrushFromBrushesBrushMenuItem := False;
end;

procedure TMainForm.afterInit(Sender: TObject; var Done: Boolean);
begin
    optionsForm.setApplayOptionsCallback(@self.applyOptions);
    //BrushEditorForm.setCallbacks(@TMainForm.addBrushIntoBrushesBrushMenuItem, @TMainForm.deleteBrushFromBrushesBrushMenuItem);
    BrushEditorForm.setAddMenuItemCallback(@self.addBrushIntoBrushesBrushMenuItem);
    BrushEditorForm.setDelMenuItemCallback(@self.deleteBrushFromBrushesBrushMenuItem);
    Application.OnIdle := nil;
end;

(*--------------------------------------------------------*)
// options
(*--------------------------------------------------------*)

procedure TMainForm.applyOptions();
begin
    MainForm.FieldsDrawGrid.ColCount := programOptions.xSize;
    MainForm.FieldsDrawGrid.RowCount := programOptions.ySize;
    MainForm.FieldsDrawGrid.DefaultColWidth := programOptions.fieldSize;
    MainForm.FieldsDrawGrid.DefaultRowHeight := programOptions.fieldSize;

    if (programOptions.isDrawOnScroll) then
        MainForm.FieldsDrawGrid.Options := MainForm.FieldsDrawGrid.Options+[goThumbTracking]
    else
        MainForm.FieldsDrawGrid.Options := MainForm.FieldsDrawGrid.Options-[goThumbTracking];

    if (programOptions.isShowCoordinates) then begin
        MainForm.FieldsDrawGrid.OnMouseMove := @FieldsDrawGridMouseMove;
        yCoordLabel.Visible := True;
        doublepointLabel.Visible := True;
        xCoordLabel.Visible := True;
    end
    else begin
        MainForm.FieldsDrawGrid.OnMouseMove := nil;
        xCoordLabel.Visible := False;
        doublepointLabel.Visible := False;
        yCoordLabel.Visible := False;
    end;

    if (programOptions.isShowToolbar) then
        MainForm.ToolBar1.Visible := True
    else
        MainForm.ToolBar1.Visible := False;
    if (programOptions.toolbarPosition = 1) then
        MainForm.ToolBar1.Align := alTop
    else
        MainForm.ToolBar1.Align := alBottom;

    Timer1.Interval := programOptions.delay_ms;

    gof.setNewSize(programOptions.ySize,programOptions.xSize);

    FieldsDrawGrid.Repaint;
end;

end.

