unit optionsUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, IniFiles;

const
  optionsFilename = 'options.ini';

  defaultWidth = 389;
  defaultHeight = 436;

  defaultDelay = 1000; // ms

  defaultxSize = 25;
  defaultySize = 25;

  defaultFieldSize = 15;

  defaultCellColor = 65280;
  defaultEmptyColor = 15790320;

type

  { Options }

  Options = class(TObject)
  private
    FcellColor: integer;
    Fdelay_ms: integer;
    FemptyColor: integer;
    FfieldSize: integer;
    FformHeight: integer;
    FformWidth: integer;
    FisDrawOnScroll: boolean;
    FisShowCoordinates: boolean;
    FisShowToolbar: boolean;
    FtoolbarPosition: integer;
    FxSize: integer;
    FySize: integer;


    optionsFile : TIniFile;
    procedure SetcellColor(AValue: integer);
    procedure Setdelay_ms(AValue: integer);
    procedure SetemptyColor(AValue: integer);
    procedure SetfieldSize(AValue: integer);
    procedure SetformHeight(AValue: integer);
    procedure SetformWidth(AValue: integer);
    procedure SetisDrawOnScroll(AValue: boolean);
    procedure SetisShowCoordinates(AValue: boolean);
    procedure SetisShowToolbar(AValue: boolean);
    procedure SettoolbarPosition(AValue: integer);
    procedure SetxSize(AValue: integer);
    procedure SetySize(AValue: integer);

  public
    constructor Create();
    destructor Destroy(); override;

    procedure readOptions();
    procedure saveOptions();
    procedure saveWindowSize(x,y : integer);
    procedure createOptions();

    property xSize : integer read FxSize write SetxSize;
    property ySize : integer read FySize write SetySize;

    property delay_ms : integer read Fdelay_ms write Setdelay_ms;

    property fieldSize : integer read FfieldSize write SetfieldSize;

    property isShowCoordinates : boolean read FisShowCoordinates write SetisShowCoordinates;
    property isDrawOnScroll : boolean read FisDrawOnScroll write SetisDrawOnScroll;

    property cellColor : integer read FcellColor write SetcellColor;
    property emptyColor : integer read FemptyColor write SetemptyColor;

    property isShowToolbar : boolean read FisShowToolbar write SetisShowToolbar;
    property toolbarPosition : integer read FtoolbarPosition write SettoolbarPosition; { 0 - top, 1 - right, 2 - bottom, 3 - left }

    property formWidth : integer read FformWidth write SetformWidth;
    property formHeight : integer read FformHeight write SetformHeight;

  end;

  var
      programOptions : Options;
implementation

{ Options }

procedure Options.readOptions();
begin
    optionsFile:=TiniFile.Create(extractfilepath(paramstr(0))+optionsFilename);

    self.xSize := optionsFile.ReadInteger('Config','x',defaultxSize);
    self.ySize := optionsFile.ReadInteger('Config','y',defaultySize);

    self.delay_ms := optionsFile.ReadInteger('Config','pause',defaultDelay);

    self.fieldSize := optionsFile.ReadInteger('View','fieldSize',defaultFieldSize);

    self.isShowCoordinates := optionsFile.ReadBool('View','showCoordinates',True);
    self.isDrawOnScroll := optionsFile.ReadBool('View','drawOnScroll',True);

    self.cellColor := optionsFile.ReadInteger('View','cellColor',defaultCellColor);
    self.emptyColor := optionsFile.ReadInteger('View','emptyColor',defaultEmptyColor);

    self.isShowToolbar := optionsFile.ReadBool('View','showToolbar',True);
    self.toolbarPosition := optionsFile.ReadInteger('View','toolbarPosition',1);

    self.formHeight := optionsFile.ReadInteger('View','height',defaultHeight);
    self.formWidth := optionsFile.ReadInteger('View','width',defaultWidth);

    optionsFile.Free;
end;

procedure Options.saveOptions();
begin
    optionsFile:=TiniFile.Create(extractfilepath(paramstr(0))+optionsFilename);

    optionsFile.WriteInteger('Config','x',self.xSize);
    optionsFile.WriteInteger('Config','y',self.ySize);

    optionsFile.WriteInteger('Config','pause',self.delay_ms);

    optionsFile.WriteInteger('View','fieldSize',self.fieldSize);

    optionsFile.WriteBool('View','showCoordinates',self.isShowCoordinates);
    optionsFile.WriteBool('View','drawOnScroll',self.isDrawOnScroll);

    optionsFile.WriteInteger('View','cellColor',self.cellColor);
    optionsFile.WriteInteger('View','emptyColor',self.emptyColor);

    optionsFile.WriteBool('View','showToolbar',self.isShowToolbar);
    optionsFile.WriteInteger('View','toolbarPosition',self.toolbarPosition);

    optionsFile.WriteInteger('View','height',self.formHeight);
    optionsFile.WriteInteger('View','width',self.formWidth);

    optionsFile.Free;
end;

procedure Options.saveWindowSize(x,y : integer);
begin
    optionsFile:=TiniFile.Create(extractfilepath(paramstr(0))+optionsFilename);
    optionsFile.WriteInteger('View','height',y);
    optionsFile.WriteInteger('View','width', x);
    optionsFile.Free;
end;

procedure Options.createOptions();
begin
    optionsFile:=TiniFile.Create(extractfilepath(paramstr(0))+optionsFilename);

    optionsFile.WriteInteger('Config','x',defaultxSize);
    optionsFile.WriteInteger('Config','y',defaultySize);

    optionsFile.WriteInteger('Config','pause',defaultDelay);

    optionsFile.WriteInteger('View','fieldSize',defaultFieldSize);

    optionsFile.WriteBool('View','showCoordinates',True);
    optionsFile.WriteBool('View','drawOnScroll',True);

    optionsFile.WriteInteger('View','cellColor',defaultCellColor);
    optionsFile.WriteInteger('View','emptyColor',defaultEmptyColor);

    optionsFile.WriteBool('View','showToolbar',True);
    optionsFile.WriteInteger('View','toolbarPosition',1);

    optionsFile.WriteInteger('View','height',defaultHeight);
    optionsFile.WriteInteger('View','width',defaultWidth);

    optionsFile.Free;
end;

///////////////////////////////////////////////

procedure Options.SetcellColor(AValue: integer);
begin
  if FcellColor=AValue then Exit;
  FcellColor:=AValue;
end;

procedure Options.Setdelay_ms(AValue: integer);
begin
  if Fdelay_ms=AValue then Exit;
  Fdelay_ms:=AValue;
end;

procedure Options.SetemptyColor(AValue: integer);
begin
  if FemptyColor=AValue then Exit;
  FemptyColor:=AValue;
end;

procedure Options.SetfieldSize(AValue: integer);
begin
  if FfieldSize=AValue then Exit;
  FfieldSize:=AValue;
end;

procedure Options.SetformHeight(AValue: integer);
begin
  if FformHeight=AValue then Exit;
  FformHeight:=AValue;
end;

procedure Options.SetformWidth(AValue: integer);
begin
  if FformWidth=AValue then Exit;
  FformWidth:=AValue;
end;

procedure Options.SetisDrawOnScroll(AValue: boolean);
begin
  if FisDrawOnScroll=AValue then Exit;
  FisDrawOnScroll:=AValue;
end;

procedure Options.SetisShowCoordinates(AValue: boolean);
begin
  if FisShowCoordinates=AValue then Exit;
  FisShowCoordinates:=AValue;
end;

procedure Options.SetisShowToolbar(AValue: boolean);
begin
  if FisShowToolbar=AValue then Exit;
  FisShowToolbar:=AValue;
end;

procedure Options.SettoolbarPosition(AValue: integer);
begin
  if FtoolbarPosition=AValue then Exit;
  FtoolbarPosition:=AValue;
end;

procedure Options.SetxSize(AValue: integer);
begin
  if FxSize=AValue then Exit;
  FxSize:=AValue;
end;

procedure Options.SetySize(AValue: integer);
begin
  if FySize=AValue then Exit;
  FySize:=AValue;
end;

constructor Options.Create;
begin

end;

destructor Options.Destroy;
begin
  inherited Destroy;
end;

end.

