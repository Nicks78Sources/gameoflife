unit brushes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  game;

const
  brushesFilename = 'brushes.txt';

type

  { CellBrushes }

  CellBrushes = class(TObject)
  public
    constructor Create();
    destructor Destroy();

    procedure loadFromFile();
    procedure saveToFile();
    function saveBrushInString(brush : GameOfLife) : String;

    function addBrush(s : String) : boolean;
    function addBrush(b : GameOfLife) : boolean;
    function deleteBrush(brushName : String) : boolean;
    function deleteBrush(brushIndex : integer) : boolean;
    function getBrush(i : integer) : GameOfLife;
    function getBrushNumber(brushName : String) : integer;
    function getBrushQuantity() : integer;

    procedure paint(blank : GameOfLife; brushNumber : integer; x,y : integer; angle : integer);
    procedure paintEx(blank : GameOfLife; brushNumber : integer; x,y : integer; angle : integer; xSimmetry,ySimmetry : boolean);
    procedure printExByCode(blank: GameOfLife; brushNumber: integer; x,y: integer; code : integer);
    //procedure paint(brushName : String);
  protected
    brushList : TList;

    procedure addBaseBrushes();
  end;

  var
    GameBrushes : CellBrushes;


implementation

{ CellBrushes }

constructor CellBrushes.Create;
begin
    self.brushList := TList.Create;
end;

destructor CellBrushes.Destroy;
var
  i : integer;
begin
    for i:=0 to self.brushList.Count do
        GameOfLife(self.brushList[i]).Free;
    self.brushList.Free;
end;

procedure CellBrushes.loadFromFile();
var
  brushStringList : TStringList;
  s : String;
begin
    brushStringList := TStringList.Create;
    try
       brushStringList.LoadFromFile(brushesFilename);
    except
       self.addBaseBrushes();
    end;
    brushStringList.Sort();

    for s in brushStringList do begin
         self.addBrush(s);
    end;
    brushStringList.Free;

    if (GameBrushes.getBrushQuantity() = 0) then
       self.addBaseBrushes();
end;

procedure CellBrushes.saveToFile;
var
  brushStringList : TStringList;
  i               : integer;
begin
    brushStringList := TStringList.Create;
    for i:=0 to GameBrushes.getBrushQuantity()-1 do
        brushStringList.Add(self.saveBrushInString(GameBrushes.getBrush(i)));
    brushStringList.SaveToFile(brushesFilename);
    brushStringList.Free;
end;

function CellBrushes.saveBrushInString(brush: GameOfLife): String;
var
  brushHeader, brushData  : String;
  i,j,k  : integer;
begin
    brushHeader := brush.name + ':' + IntToStr(brush.ySize) + '*' + IntToStr(brush.xSize) + ';';
    SetLength(brushData, brush.xSize * brush.ySize);
    k := 1;
    for i:=0 to brush.ySize-1 do
        for j:=0 to brush.xSize-1 do begin
            brushData[k] := Chr(brush.field[i,j] + ord('0'));
            Inc(k);
        end;
    saveBrushInString := Concat(brushHeader,brushData);
end;

function CellBrushes.addBrush(s: String): boolean;
var
  curBrush : GameOfLife;

  sd    : String;
  len   : integer;
  k     : integer;
  doublepointPos, starPos, semicolonPos : integer;
begin
    curBrush := GameOfLife.Create(0,0);
    // name:y*x;10101010...
    try
        doublepointPos := Pos(':',s);
        starPos := Pos('*',s);
        semicolonPos := Pos(';',s);

        curBrush.name := Copy(s,1,Pos(':',s)-1);
        curBrush.ySize := StrToInt(Copy(s, doublepointPos+1, starPos - doublepointPos-1));
        curBrush.xSize := StrToInt(Copy(s, starPos+1, semicolonPos - starPos-1));
        sd := Copy(s, semicolonPos+1, Length(s) - semicolonPos);

        len := Length(sd);
        if (len <> (curBrush.ySize * curBrush.xSize)) then begin
            raise Exception.Create('');
        end;
    except
        curBrush.Free;
        addBrush := False;
        exit;
    end;

    curBrush.setNewSize(curBrush.ySize,curBrush.xSize);
    for k:=0 to len-1 do begin
        curBrush.field[k div curBrush.xSize, k mod curBrush.xSize] := ord(sd[k+1]) - ord('0');
    end;
    self.brushList.Add(Pointer(curBrush));
    addBrush := True;
end;

function CellBrushes.addBrush(b: GameOfLife): boolean;
begin
    self.brushList.Add(b);
    addBrush := True;
end;

function CellBrushes.deleteBrush(brushName: String): boolean;
var
  i : integer;
begin
    for i:=0 to self.brushList.Count do begin
         if (self.getBrush(i).name = brushName) then begin
             self.getBrush(i).Free;
             self.brushList.Delete(i);
             deleteBrush := True;
             exit;
         end;
    end;
    deleteBrush := False;
end;

function CellBrushes.deleteBrush(brushIndex: integer): boolean;
begin
    if ((brushIndex >= 0) and (brushIndex < self.getBrushQuantity())) then begin
        GameOfLife(self.brushList[brushIndex]).Free;
        self.brushList.Delete(brushIndex);
        deleteBrush := True;
    end
    else
        deleteBrush := False;
end;

function CellBrushes.getBrush(i: integer): GameOfLife;
begin
    getBrush := GameOfLife(self.brushList[i]);
end;

function CellBrushes.getBrushNumber(brushName: String): integer;
var
  i : integer;
begin
    for i:= 0 to self.getBrushQuantity()-1 do
        if (GameOfLife(self.brushList[i]).name = brushName) then begin
            getBrushNumber := i;
            exit;
        end;
    getBrushNumber := -1;
end;

function CellBrushes.getBrushQuantity: integer;
begin
    getBrushQuantity := self.brushList.Count;
end;

procedure CellBrushes.paint(blank: GameOfLife; brushNumber: integer; x,y : integer; angle: integer);
var
  i,j : integer;
  brush : GameOfLife;
begin
    brush := GameOfLife(self.brushList[brushNumber]);
    case angle of
    0:
        for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+i,y+j,brush.getUnitAt(i,j));
    90:
         for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+j,y+brush.ySize-1-i,brush.getUnitAt(i,j));
    180:
         for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+brush.xSize-1-i,y+brush.ySize-1-j,brush.getUnitAt(i,j));
    270:
         for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+brush.xSize-1-j,y+i,brush.getUnitAt(i,j));
    end;
end;

// we have only 8 different position of figure!
procedure CellBrushes.paintEx(blank: GameOfLife; brushNumber: integer; x,
  y: integer; angle: integer; xSimmetry, ySimmetry: boolean);
const
  normalizeCode : array[0..15] of integer =(0,1,2,3,4,5,6,7, 6,7,4,5,2,3,0,1);
var
  code : integer;
begin
    code := (angle div 90);
    if (xSimmetry) then
       code := code + 4;
    if (ySimmetry) then
       code := code + 8;

    code := normalizeCode[code];

    printExByCode(blank,brushNumber,x,y,code);
end;

procedure CellBrushes.printExByCode(blank: GameOfLife; brushNumber: integer; x,y: integer; code : integer);
var
  i,j : integer;
  brush : GameOfLife;
begin
    brush := GameOfLife(self.brushList[brushNumber]);

    case code of
    0:
        for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+i,y+j,brush.getUnitAt(i,j));
    1:
         for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+j,y+brush.ySize-1-i,brush.getUnitAt(i,j));
    2:
         for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+brush.ySize-1-i,y+brush.xSize-1-j,brush.getUnitAt(i,j));
    3:
         for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+brush.xSize-1-j,y+i,brush.getUnitAt(i,j));
    4:
         for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+i,y+brush.xSize-1-j,brush.getUnitAt(i,j));
    5:
         for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+brush.xSize-1-j,y+brush.ySize-1-i,brush.getUnitAt(i,j));
    6:
         for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+brush.ySize-1-i,y+j,brush.getUnitAt(i,j));
    7:
         for i:=0 to brush.ySize-1 do
            for j:=0 to brush.xSize-1 do
                blank.setUnitAt(x+j,y+i,brush.getUnitAt(i,j));
    end;
end;

procedure CellBrushes.addBaseBrushes();
begin
     GameBrushes.addBrush('Cell:1*1;1');
     GameBrushes.addBrush('Empty:1*1;0');
end;

end.

