unit game;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  gameField = array of array of integer;

type

  { GameOfLife }

  GameOfLife = class(TObject)
  public
      field    : gameField;        // 0 - empty, 1 - cell
      {oldField : gameField;
      swap     : gameField;}
      xSize : integer;
      ySize : integer;
      name  : String;

      numberOfSteps : integer;

      constructor Create(y,x : integer);
      destructor Destroy();

      procedure step();
      function getNumberOfSteps() : integer;
      procedure resetStepsCounter();

      function getUnitAt(i,j : integer) : integer;
      procedure setUnitAt(i,j : integer; value : integer);
      procedure invertFieldAt(i,j : integer);
      procedure clearField();
      procedure randomFill(percent : integer);

      function getXSize() : integer;
      function getYSize() : integer;

      procedure setNewSize(newY,newX : integer);

  protected
      procedure normalizeCoordinates(var i,j : integer);
      procedure normalizeCoordinate(var c : integer; module : integer);

      function getNeighborsQuantity(i,j : integer) : integer;
  end;

implementation

{ GameOfLife }

constructor GameOfLife.Create(y, x: integer);
begin
    {if (x < 3) then x := 3;
    if (y < 3) then y := 3;}
    SetLength(self.field,y,x);
    self.xSize := x;
    self.ySize := y;
end;

destructor GameOfLife.Destroy;
begin
    SetLength(self.field,0,0);
    self.field := nil;
end;

{
  1 - cell,
  0 - empty

  2 - new cell
 -1 - dead cell

  2 -> 1
 -1 -> 0
}
procedure GameOfLife.step();
var
  i,j : integer;
  nq  : integer;
begin
    Inc(self.numberOfSteps);

    for i:=0 to self.ySize-1 do
        for j:=0 to self.xSize-1 do begin
            nq := self.getNeighborsQuantity(i,j);
            if (self.field[i,j] = 0) then begin    // current field is empty
                if (nq = 3) then
                    self.field[i,j] := 2;          // new cell
            end
            else begin                             // curren field is cell
                if ((nq <> 2) and (nq <> 3)) then
                    self.field[i,j] := -1;         // dead cell
            end;
        end;

    // 2 -> 1; -1 -> 0
    for i:=0 to self.ySize-1 do
        for j:=0 to self.xSize-1 do begin
            if (self.field[i,j] = -1) then
                Inc(self.field[i,j])               // -1 -> 0
            else if (self.field[i,j] = 2) then
                Dec(self.field[i,j]);              //  2 -> 1
        end;
end;

function GameOfLife.getNumberOfSteps(): integer;
begin
    getNumberOfSteps := self.numberOfSteps;
end;

procedure GameOfLife.resetStepsCounter;
begin
    self.numberOfSteps := 0;
end;

function GameOfLife.getUnitAt(i, j: integer): integer;
begin
    self.normalizeCoordinates(i,j);
    getUnitAt := self.field[i,j];
end;

procedure GameOfLife.setUnitAt(i, j: integer; value : integer);
begin
    self.normalizeCoordinates(i,j);
    if (value = 0) then
        self.field[i,j] := 0
    else
        self.field[i,j] := 1;
end;

procedure GameOfLife.invertFieldAt(i, j: integer);
begin
  self.normalizeCoordinates(i,j);
  if (self.field[i,j] = 0) then
      self.field[i,j] := 1
  else
      self.field[i,j] := 0;
end;

procedure GameOfLife.clearField;
var
  i,j : integer;
begin
    for i:=0 to self.ySize-1 do
        for j:=0 to self.xSize-1 do
            self.field[i,j] := 0;
end;

procedure GameOfLife.randomFill(percent: integer);
var
  probability : real;
  i,j         : integer;
begin
    probability := percent / 100;
    for i:=0 to self.ySize-1 do
        for j:=0 to self.xSize-1 do
            if (Random() < probability) then
                self.field[i,j] := 1;
            {else
                self.field[i,j] := 0;}
end;

function GameOfLife.getXSize: integer;
begin
    getXSize := self.xSize;
end;

function GameOfLife.getYSize: integer;
begin
    getYSize := self.ySize;
end;

procedure GameOfLife.setNewSize(newY, newX: integer);
begin
    //if ((newX < 3) or (newY < 3)) then Exit;
    SetLength(self.field,newY,newX);
    self.xSize := newX;
    self.ySize := newY;
end;

// (i,j) -> i in [0..ySize-1], j in [0..xSize-1]
procedure GameOfLife.normalizeCoordinates(var i, j: integer);
begin
    self.normalizeCoordinate(i,self.ySize);
    self.normalizeCoordinate(j,self.xSize);
end;

procedure GameOfLife.normalizeCoordinate(var c: integer; module : integer);
begin
  if (c < 0) then
      while (c < 0) do
          c := c + module
  else
      c := c mod module;
end;

function GameOfLife.getNeighborsQuantity(i, j: integer): integer;
var
  q : integer;
begin
    q := 0;
    if (abs(self.getUnitAt(i+1,j)) = 1) then Inc(q);
    if (abs(self.getUnitAt(i-1,j)) = 1) then Inc(q);
    if (abs(self.getUnitAt(i,j+1)) = 1) then Inc(q);
    if (abs(self.getUnitAt(i,j-1)) = 1) then Inc(q);
    if (abs(self.getUnitAt(i+1,j+1)) = 1) then Inc(q);
    if (abs(self.getUnitAt(i-1,j-1)) = 1) then Inc(q);
    if (abs(self.getUnitAt(i+1,j-1)) = 1) then Inc(q);
    if (abs(self.getUnitAt(i-1,j+1)) = 1) then Inc(q);
    getNeighborsQuantity := q;
end;

end.

