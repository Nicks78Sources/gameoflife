unit fillDialog;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Spin,
  StdCtrls;

type

  { TfillDialogForm }

  TfillDialogForm = class(TForm)
    clearBeforeFillCheckBox: TCheckBox;
    okButton: TButton;
    cancelButton: TButton;
    Label1: TLabel;
    fillProbabilitySpinEdit: TSpinEdit;
    procedure cancelButtonClick(Sender: TObject);
    procedure okButtonClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    probabilityPercents   : integer;
    isClearFieldBeforeFill : boolean;
    confirmation           : boolean;
  end;

var
  fillDialogForm: TfillDialogForm;

implementation

{$R *.lfm}

{ TfillDialogForm }

procedure TfillDialogForm.okButtonClick(Sender: TObject);
begin
    self.confirmation := True;
    self.probabilityPercents := fillProbabilitySpinEdit.Value;
    self.isClearFieldBeforeFill := clearBeforeFillCheckBox.Checked;
    fillDialogForm.Close();
end;

procedure TfillDialogForm.cancelButtonClick(Sender: TObject);
begin
    self.confirmation := False;
    fillDialogForm.Close();
end;


end.

