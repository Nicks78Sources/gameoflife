unit stepsDialog;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Spin;

type

  { TstepsDialogForm }

  TstepsDialogForm = class(TForm)
    okButton: TButton;
    cancelButton: TButton;
    viewProcessCheckBox: TCheckBox;
    Label1: TLabel;
    stepsSpinEdit: TSpinEdit;
    procedure cancelButtonClick(Sender: TObject);
    procedure okButtonClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    steps : integer;
    isViewing : boolean;
    confirmation : boolean;
  end;

var
  stepsDialogForm: TstepsDialogForm;

implementation

{$R *.lfm}

{ TstepsDialogForm }

procedure TstepsDialogForm.cancelButtonClick(Sender: TObject);
begin
    self.confirmation := False;
    stepsDialogForm.Close();
end;

procedure TstepsDialogForm.okButtonClick(Sender: TObject);
begin
    self.confirmation := True;
    self.steps := stepsSpinEdit.Value;
    self.isViewing := viewProcessCheckBox.Checked;
    stepsDialogForm.Close();
end;

end.

