unit aboutFormUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, ExtCtrls;

type

  { TaboutForm }

  TaboutForm = class(TForm)
    Button1: TButton;
    infoImage: TImage;
    infoLabel: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure infoImageClick(Sender: TObject);
    procedure infoLabelClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  aboutForm: TaboutForm;

implementation

{$R *.lfm}

{ TaboutForm }

procedure TaboutForm.Button1Click(Sender: TObject);
begin
  aboutForm.Hide();
end;

procedure TaboutForm.FormCreate(Sender: TObject);
begin
    infoImage.Visible := True;
    infoLabel.Visible := False;
end;

procedure TaboutForm.infoImageClick(Sender: TObject);
begin
    infoImage.Visible := False;
    infoLabel.Visible := True;
end;

procedure TaboutForm.infoLabelClick(Sender: TObject);
begin
    infoLabel.Visible := False;
    infoImage.Visible := True;
end;

end.

