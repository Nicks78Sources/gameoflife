unit brushEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids,
  StdCtrls, Spin,
  brushes,
  game,
  optionsUnit;

type
   CallbackMenuFunction = function(brushName : String) : boolean of object;

type

  { TBrushEditorForm }

  TBrushEditorForm = class(TForm)
    brushDrawGrid: TDrawGrid;
    addBrushButton: TButton;
    deleteBrushButton: TButton;
    saveBrushButton: TButton;
    BrushesComboBox: TComboBox;
    widthSpinEdit: TSpinEdit;
    heightSpinEdit: TSpinEdit;
    procedure addBrushButtonClick(Sender: TObject);
    procedure brushDrawGridClick(Sender: TObject);
    procedure brushDrawGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure BrushesComboBoxChange(Sender: TObject);
    procedure deleteBrushButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure saveBrushButtonClick(Sender: TObject);
    procedure SpinChange(Sender : TObject);

    //procedure setCallbacks(a,d : CallbackMenuFunction);
    procedure setAddMenuItemCallback(a : CallbackMenuFunction);
    procedure setDelMenuItemCallback(d : CallbackMenuFunction);
  private
    addInMenu : CallbackMenuFunction;
    delFromMenu : CallbackMenuFunction;
  end;

var
  BrushEditorForm: TBrushEditorForm;
  pattern : GameOfLife;

implementation

{$R *.lfm}

{ TBrushEditorForm }

procedure TBrushEditorForm.FormCreate(Sender: TObject);
var
  i : integer;
  n : integer;
begin
    BrushEditorForm.brushDrawGrid.DefaultColWidth := programOptions.fieldSize;
    BrushEditorForm.brushDrawGrid.DefaultRowHeight := programOptions.fieldSize;

    n := GameBrushes.getBrushQuantity()-1;
    for i:=0 to n do begin
        BrushEditorForm.BrushesComboBox.Items.Add(GameBrushes.getBrush(i).name);
    end;
    BrushEditorForm.BrushesComboBox.ItemIndex := 0;
    BrushEditorForm.BrushesComboBox.OnChange(nil);
end;

procedure TBrushEditorForm.FormDestroy(Sender: TObject);
begin
    pattern.Free();
end;

procedure TBrushEditorForm.FormShow(Sender: TObject);
begin
    if (BrushEditorForm.Left < 0) then BrushEditorForm.Left := 0;
    if (BrushEditorForm.Top < 0) then BrushEditorForm.Top := 0;
end;

{procedure TBrushEditorForm.setCallbacks(a,d : CallbackMenuProcedure);
begin
    self.addInMenu := a;
    self.delFromMenu := d;
end;}

procedure TBrushEditorForm.setAddMenuItemCallback(a: CallbackMenuFunction);
begin
    self.addInMenu := a;
end;

procedure TBrushEditorForm.setDelMenuItemCallback(d: CallbackMenuFunction);
begin
    self.delFromMenu := d;
end;

//------------------------------------------------------
// draw

procedure TBrushEditorForm.brushDrawGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
begin
  with Sender as TDrawGrid, Canvas do begin
    if (pattern.getUnitAt(aRow,aCol) = 1) then
        Canvas.Brush.Color := programOptions.cellColor
    else
        Canvas.Brush.Color := programOptions.emptyColor;

    Canvas.FillRect(aRect);
  end;
end;

procedure TBrushEditorForm.brushDrawGridClick(Sender: TObject);
var
  cur : TGridRect;
  row,col : integer;
begin
    cur := brushDrawGrid.Selection;
    for row := cur.Top to cur.Bottom do
        for col := cur.Left to cur.Right do begin
            pattern.invertFieldAt(row,col);
        end;
end;

(*--------------------------------------------------------*)

procedure TBrushEditorForm.SpinChange(Sender : TObject);
begin
    pattern.setNewSize(heightSpinEdit.Value, widthSpinEdit.Value);
    BrushEditorForm.brushDrawGrid.ColCount := widthSpinEdit.Value;
    BrushEditorForm.brushDrawGrid.RowCount := heightSpinEdit.Value;

    BrushEditorForm.brushDrawGrid.Repaint;
end;

procedure TBrushEditorForm.BrushesComboBoxChange(Sender: TObject);
var
  patternNumber : integer;
begin
    patternNumber := BrushEditorForm.BrushesComboBox.ItemIndex;
    pattern := GameBrushes.getBrush(patternNumber);

    widthSpinEdit.OnChange := nil;
    heightSpinEdit.OnChange := nil;

    widthSpinEdit.Value := pattern.getXSize();
    heightSpinEdit.Value := pattern.getYSize();
    BrushEditorForm.brushDrawGrid.ColCount := pattern.getXSize();
    BrushEditorForm.brushDrawGrid.RowCount := pattern.getYSize();

    widthSpinEdit.OnChange := @self.SpinChange;
    heightSpinEdit.OnChange := @self.SpinChange;

    BrushEditorForm.brushDrawGrid.Repaint;
end;

procedure TBrushEditorForm.addBrushButtonClick(Sender: TObject);
var
  nameOfNewBrush : String;
  newBrush       : GameOfLife;
begin
    nameOfNewBrush := InputBox('New brush','Name of new brush:','');
    if ((nameOfNewBrush = '') or (nameOfNewBrush = 'None')) then exit;

    newBrush := GameOfLife.Create(5,5);
    newBrush.name := nameOfNewBrush;
    GameBrushes.addBrush(newBrush);

    BrushEditorForm.BrushesComboBox.Items.Add(newBrush.name);
    BrushEditorForm.BrushesComboBox.ItemIndex := GameBrushes.getBrushQuantity()-1;
    BrushEditorForm.BrushesComboBox.OnChange(nil);

    addInMenu(newBrush.name);

    BrushEditorForm.deleteBrushButton.Enabled := True;
end;

procedure TBrushEditorForm.deleteBrushButtonClick(Sender: TObject);
var
  index : integer;
  brushName : String;
begin
    index := BrushEditorForm.BrushesComboBox.ItemIndex;
    BrushEditorForm.BrushesComboBox.Items.Delete(index);

    brushName := GameBrushes.getBrush(index).name;
    GameBrushes.deleteBrush(index);

    if (index = GameBrushes.getBrushQuantity()) then
        index := 0;
    BrushEditorForm.BrushesComboBox.ItemIndex := index;
    BrushEditorForm.BrushesComboBox.OnChange(nil);

    delFromMenu(brushName);

    if (GameBrushes.getBrushQuantity() = 1) then
        BrushEditorForm.deleteBrushButton.Enabled := False;
end;

procedure TBrushEditorForm.saveBrushButtonClick(Sender: TObject);
begin
    GameBrushes.saveToFile();
    BrushEditorForm.Hide();
end;

end.

