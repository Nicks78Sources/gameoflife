unit optionsFormUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Spin, ExtCtrls,
  optionsUnit;

type
  OptionsCallback = procedure() of object;

type

  { ToptionsForm }

  ToptionsForm = class(TForm)
    applayButton: TButton;
    cancelButton: TButton;
    cellColorColorButton: TColorButton;
    emptyColorColorButton: TColorButton;
    resetButton: TButton;
    showCoordinatesCheckBox: TCheckBox;
    drawOnScrollCheckBox: TCheckBox;
    toolbarCheckBox: TCheckBox;
    fieldGroupBox: TGroupBox;
    delayFloatSpinEdit: TFloatSpinEdit;
    toolbarPositionTopRadioButton: TRadioButton;
    toolbarPositionBottomRadioButton: TRadioButton;
    toolbarPositionRadioGroup: TRadioGroup;
    toolbarGroupBox: TGroupBox;
    fieldsizeLabel: TLabel;
    delayLabel: TLabel;
    cellColorLabel: TLabel;
    emptyColorLabel: TLabel;
    cellsizeSpinEdit: TSpinEdit;
    viewGroupBox: TGroupBox;
    heightLabel: TLabel;
    widthLabel: TLabel;
    areaheightSpinEdit: TSpinEdit;
    areawidthSpinEdit: TSpinEdit;
    procedure applayButtonClick(Sender: TObject);
    procedure cancelButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure resetButtonClick(Sender: TObject);
    procedure setApplayOptionsCallback(cb : OptionsCallback);
    procedure toolbarCheckBoxChange(Sender: TObject);
  private
    apllayOptionsCallback : OptionsCallback;
  public
    { public declarations }
  end;

var
  optionsForm: ToptionsForm;

implementation

{$R *.lfm}

{ ToptionsForm }

procedure ToptionsForm.FormShow(Sender: TObject);
begin
    areaheightSpinEdit.Value          := programOptions.ySize;
    areawidthSpinEdit.Value           := programOptions.xSize;

    cellsizeSpinEdit.Value            := programOptions.fieldSize;
    delayFloatSpinEdit.Value          := programOptions.delay_ms / 1000;
    showCoordinatesCheckBox.Checked   := programOptions.isShowCoordinates;
    drawOnScrollCheckBox.Checked      := programOptions.isDrawOnScroll;
    cellColorColorButton.ButtonColor  := programOptions.cellColor;
    emptyColorColorButton.ButtonColor := programOptions.emptyColor;

    toolbarCheckBox.Checked           := programOptions.isShowToolbar;
    if (programOptions.toolbarPosition = 1) then
        toolbarPositionTopRadioButton.Checked := True
    else
        toolbarPositionBottomRadioButton.Checked := True;

    if (optionsForm.Left < 0) then optionsForm.Left := 0;
    if (optionsForm.Top < 0) then optionsForm.Top := 0;
end;

procedure ToptionsForm.applayButtonClick(Sender: TObject);
begin
    programOptions.ySize             := areaheightSpinEdit.Value;
    programOptions.xSize             := areawidthSpinEdit.Value;

    programOptions.fieldSize         := cellsizeSpinEdit.Value;
    programOptions.delay_ms          := Trunc(delayFloatSpinEdit.Value * 1000);
    if (programOptions.delay_ms = 0) then programOptions.delay_ms := 1;
    programOptions.isShowCoordinates := showCoordinatesCheckBox.Checked;
    programOptions.isDrawOnScroll    := drawOnScrollCheckBox.Checked;
    programOptions.cellColor         := cellColorColorButton.ButtonColor;
    programOptions.emptyColor        := emptyColorColorButton.ButtonColor;

    programOptions.isShowToolbar     := toolbarCheckBox.Checked;
    if (toolbarPositionTopRadioButton.Checked = True ) then
        programOptions.toolbarPosition := 1
    else
        programOptions.toolbarPosition := 2;

    programOptions.saveOptions();
    self.apllayOptionsCallback();
    optionsForm.Hide();
end;

procedure ToptionsForm.cancelButtonClick(Sender: TObject);
begin
    optionsForm.Hide();
end;

procedure ToptionsForm.resetButtonClick(Sender: TObject);
begin
    programOptions.createOptions();
    programOptions.readOptions();
    optionsForm.Hide();
    optionsForm.Show();
end;

procedure ToptionsForm.setApplayOptionsCallback(cb: OptionsCallback);
begin
    self.apllayOptionsCallback := cb;
end;

//-------------------------------------------------

procedure ToptionsForm.toolbarCheckBoxChange(Sender: TObject);
begin
    if (toolbarCheckBox.Checked = True) then begin
        toolbarPositionRadioGroup.Enabled := True;
        showCoordinatesCheckBox.Enabled := True;
    end
    else begin
        toolbarPositionRadioGroup.Enabled := False;
        showCoordinatesCheckBox.Enabled := False;
    end;
end;

end.

